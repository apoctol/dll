﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ds.test.impl
{
        public class Plugins : IPluginFactory
        {
            private static Plugins instance;
            private Plugins()
            {
                GetPluginNames = new string[] { "sum", "sub", "multy", "div" };
                PluginsMass = new IPlugin[] { new Summarise("sum"), new Substract("sub"), new Multiply("multy"), new Divide("div") };
            }
            public static Plugins GetInstanse()
            {
                if (instance == null)
                {
                    instance = new Plugins();
                }
                return instance;
            }
        private static IPlugin[] PluginsMass;
        public int PluginCount { get; }
            public string[] GetPluginNames { get; }
            public IPlugin GetPlugin(string PluginName)
            {
                IPlugin result = null;
                foreach (var item in PluginsMass)
                {
                    if (item.PluginName == PluginName)
                    {
                        result = item;
                    }
                }
                return result;
            }
        }
       
    
}

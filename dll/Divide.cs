﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ds.test.impl
{
    class Divide:Plugin, IPlugin
    {
        public int Run(int in1, int in2)
        {

            return in1 / in2;
        }
        public Divide(string name)
        {
            PluginName = name;
        }

    }
}

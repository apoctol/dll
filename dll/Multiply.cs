﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ds.test.impl
{
    class Multiply:Plugin, IPlugin
    {
        public int Run(int in1, int in2)
        {
            return in1 * in2;
        }
        public Multiply(string name)
        {
            PluginName = name;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ds.test.impl
{
    abstract class Plugin
    {
        public string PluginName { get; protected set; }
        public string Version { get; protected set; }
        public System.Drawing.Image Image { get; protected set; }
        public string Describtion { get; protected set; }

    }
}
